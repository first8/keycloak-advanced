# Authentication flow

For each realm, it it is possible to configure an Authentication Flow. This specifies what steps users and clients have to take to log in or register themselves. On the left hand-side, in the menu, there is an Authentication tab, where the admin can configure this flow.

By default, authentication is configured on a per realm basis. Configuring authentication means associating a specific authentication Flow to each Authentication Binding. Authentication Bindings are the kind of actions which can be performed against Keycloak. These are:

* Authentication using the browser
* Authentication using direct access grant (using REST Call)
* Password credential reset
* User registration
* Client authentication

An Authentication Flow defines all the steps which are relevant to perform one of those bindings.

It is possible to define a new authentication flow, which is what we are going to do now. In this case, we are going add a _secret question_ authentication step.

## A look at the flows

1. Go to the `Authentication` tab. It will show the following screen:  

![Authentication Flows](images/tab-authentication.png)  

2. In the first tab `Flows` you'll see the available flows in the dropdown menu and below the steps within this flow. The default flows are simply named after the binding they implement.

3. When clicking on the `Bindings` tab we see which flow is bound to which binding:

![Authentication Bindings](images/tab-authentication-bindings.png)

## Customizing the authentication flow

Git clone the sample authenticator repo:
```
git clone https://bitbucket.org/first8/example-authenticator.git
```

Build the maven package:
```
mvn clean package
```

## Deploy

To deploy, copy `target/authenticator-required-action-example.jar` to deployments `<keycloak-dir>/standalone/deployments/` directory.

Check the keycloak console log to see if it is deployed, it should report something like:
```
09:35:29,391 INFO  [org.jboss.as.server] (DeploymentScanner-threads - 1) WFLYSRV0010: Deployed "authenticator-required-action-example.jar" (runtime-name : "authenticator-required-action-example.jar")
```

As this deployment contains templates we also need to copy those. Copy the `secret-question.ftl` and `secret-question-config.ftl` to `themes/base/login`.



## Use the new Authenticator

1. Go to the `Authentication` menu item and go to the `Flow` tab, you will be able to view the currently defined flows. You cannot modify built-in flows, so to add the Authenticator you have to copy an existing flow or create your own. Copy the `Browser` flow and give it a recognisable name like `Secret Question Flow`.

2. We want to add the secret question to the login form. So, right of `Secret Question Flow Forms`, click on `Actions` / `Add flow`.

![Authentication Flow Step Action](images/tab-authentication-flow.png)

3. Give the alias `Secret Question` and click save. Now your custom browser flow should end with the `secret question` step.
4. We want to have this flow step perform the logic as defined in the provider that we just deployed. To do that, in the newly created step, next to the newly created `Secret Question` flow click on `Actions` and click `Add Execution`.

![Authentication Flow Step Action](images/tab-authentication-action.png)

5. In the dropdown menu select `Secret Question` as provider (this is the provider that we added as a jar before).
6. Now, we want this to be a compulsory item in the form, so mark both new flow elements as `Required`.
7. Under Authentication, go to the Bindings tab.
8. Bind your new `Secret Question Flow` flow to the `Browser flow`.
9. If you now try to log in with existing users, they will fail because they don't have a secret question yet.
10. So, we have to register the secret question as a required action. Go to the `Required Actions` tab.
11. On the right hand-side click `Register`.
12. In the dropdown menu `Required Action` select the `Secret Question` item and save it.

Now we can actually use this newly defined flow.



## Test the new Authenticator

* try and see if the `janedoe` and `johndoe` users are asked for a secret question
* log out and see if the secret question is then validated.

