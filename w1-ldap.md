# User federation

By default Keycloak has 3 different user storage adapters. One is the internal (default) SQL database. 
The other two are _federated_ adapters:

* LDAP
* Kerberos

In this assignment we are going to setup an LDAP User Federation and see how we can extract more details from the LDAP environment and use those in Keycloak. 
First we need to connect ("bind") to the glauth server (using a connection url and a service user), and then tell Keycloak which (directories of) users to 
search and how to map them.

Then, we'll create a mapper to create Keycloak groups based on the LDAP groups, and then assign roles to the keycloak group.
In the Desk application we have normal users (with `desk_read` role) and admin users (with the `desk_write` role). We need to assign the role `desk_write` to the Keycloak users based on the LDAP group `desk-admin`. 
If you don't need to import the groups, and instead only require the roles, there is an additional exercise at the end which does that.




## LDAP

As said before, one of the federated storage adapters is LDAP. With LDAP the users will be synchronized to the Keycloak local storage, except for passwords. Password validation is delegated to the LDAP server.

Synchronization can be done manually or scheduled in a background task.

Advantages:

* You can store extra attributes/information per user in Red Hat SSO, that are not in the LDAP system.
* It reduces load on the LDAP server, as the 2nd time users are accessed they are loaded from the Keycloak Database.
* The only load on your LDAP server is password validation (and synchronization which you can plan).

Disadvantages:

* Import needs to be synchronized with LDAP changes.
* When first queried the chosen approach requires Keycloak to create records in its database which makes it a bit slower.

## Setup an LDAP server

We will use GLAuth as a light-weight LDAP server (is that a light-weight light-weight directory access protocol server then?). The charm of this server is that you can easily edit its directory configuration. 

* Download the `glauth` binary from https://github.com/glauth/glauth/releases (we have tested with version 2.2)
* Find the `glauth-conf.cfg` in the assignments that you cloned
* Run it:
    * On unix-like systems: `chmod +x glauth; glauth -c glauth-conf.cfg`
    * On Macs: `chmod +x glauthOSX; glauthOSX -c glauth-conf.cfg`
    * On Windows: rename the binary to `glauth.exe`. Then run it in the Command Prompt with `glauth -c glauth-conf.cfg`
    * If you run into trouble, see some additional tips below
* Note that glauth will log the port it is listening on; you may need this later:
```
10:28:20.296711 startLDAP ▶ NOTI 004 LDAP server listening on localhost:3893 
```


### Setup LDAP connection

1. Navigate to Keycloak Console: http://localhost:8080/auth/admin/ (for wildfly) or http://localhost:8080/admin/ (for quarkus)
2. Select the `workshop` realm
3. In `User Federation`, select`Add Ldap providers`.

![image](images/keycloak-ldap.png)

4. Provide the required LDAP configuration details (see section below for more information).


|Field|Value|Notes|
|---|---|---|
| *General options* | | |
| Vendor | `Active Directory` | The LDAP provider you are using |
| *Connection and authentication* | | |
| Connection URL |`ldap://localhost:3893` | If this does not work, check the _glauth_ console for the correct value, it logs something like `LDAP server listening on localhost:3893` and you need to prepend the ldap protocol|
| Bind Type | `simple` |  |
| Bind DN | `cn=serviceuser,ou=svcaccts,dc=first8,dc=com` | DN of the administrative or service user that accesses the information to use. Note that this user is in a different node.  |
| Bind Credential | `mysecret` |Credential/Password used for the connection |
| *LDAP searcing and updating* | | |
| Edit Mode | `READ_ONLY` |   |
| Users DN | `ou=users,dc=first8,dc=com`  | The full DN of the LDAP tree where your users are located. The DN is the LDAP user parent. This is *not* a group DN. You must specify a node that contains users. |
| Username LDAP attribute | `cn` | Attribute that contains the user name, in our case leave as `cn` |
| RDN LDAP | `cn` |   |
| UUID LDAP Attribute | `uidNumber` | Attribute in LDAP that holds a unique value. In our case set to `uidnumber` |
| User Object Classes|`posixAccount`| glauth has 'posixAccount' objects, other common names are `person, organizationalPerson, user`|
| User LDAP Filter | _leave empty_ | Used to filter the full list of users in the Users DN node to just the users you want to import into Keycloak. Can use a filter like (mail=*) to only include users with an email address. |
| Search Scope | `Subtree` |Subtree or One Level |   


5. Click `Save`
6. Navigate back to your ldap federation and on the top-right action menu, choose 'Sync all users'.
7. Check Keycloak's *Users* menu to verify they were imported correctly: Enter `*` in the search bar to see all users. You should have Jane and John Doe, and if you did not filter on e-mail, also a *serviceuser* and *administrator*.
8. Check the logs as well; Keycloak should log how many users it synchronised.

### Map LDAP Groups to Keycloak Roles

In this section we are going to import groups as defined in LDAP into Keycloak. 

### Create a Keycloak Role

(You may already have these roles from previous exercises or imports.)

1. Go to `Realm roles`
2. Click on the button `Create role`
3. Give it the name `desk_write`
4. Also create the role `desk_read`

### Import LDAP Groups

1. Go to `User Federation` and select the ldap federation you just created.
2. Select the tab `Mappers`.   
3. Select the button `Add mapper`
4. Give it a name i.e. `group ldap mapper`
5. Select Mapper Type: `group-ldap-mapper`

![image](images/ldap-mappers.png)


|Field|Value|Notes|
|---|---|---|
| LDAP Groups DN | `ou=groups,dc=first8,dc=com` | The query to use for finding groups |
| Group Name LDAP Attribute | `uid` | The attribute on the group to use as a name |
| Group Object Classes | `groupOfUniqueNames` | The object class as used for groups |
| Membership LDAP Attribute | `uniqueMember` | The attribute on the group that refers to a member |
| Membership Attribute Type | `DN` | The type of reference from a group to a member |
| User Groups Retrieve Strategy | `LOAD_GROUPS_BY_MEMBER_ATTRIBUTE` | The strategy to load the groups and users (start from a group in this case)|
| Member-Of LDAP Attribute | `memberOf` | The attribute on the user that references the group (used in `LOAD_GROUPS_BY_MEMBER_ATTRIBUTE` strategy)  |


Note: if you get weird errors (by importing or by viewing groups), you probably picked a wrong Group Name LDAP Attribute. Keycloak doesn't handle null values there well, so you will need to delete the corrupted groups. Easiest way is to turn ON
`Drop non-existing groups during sync`, making sure that Keycloak starts off with a clean sheet everytime.

6. Click `Save`
7. Select `Sync LDAP Groups to Keycloak`
8. Using the 'Users' and 'Groups' menu items, check that the user `administrator` is in the group '/desk-admin'

### Map Group to Role
1. Go to `Groups`
2. Select the LDAP group `deskusers`
3. Select `Role Mappings`
4. Select the `desk_read` and click on `Assign`
5. Verify that the users Jane and John Doe now have the role `desk_read`. (Hint: you may need to view inherited roles).
6. Select the LDAP group `desk-admin`
7. Select `Role Mappings`
8. Select the `desk_write` and click on `Assign`
9. Verify that the user `administrator` now has both roles `desk_read` and `desk_write`. (Hint: the administator is in both groups).


### Conclusion
You have now imported groups from LDAP into groups in Keycloak. And, for a specific group (the `desk-admin` group), you have assigned the Role `desk_write`. This special group can now use its priviliged permissions in our Desk app. 

### Testing with the app
You should now be able to test this with the Desk app, http://localhost:8000

* The users Jane and John Doe (`janedoe` and `johndoe`, passwords are `dogood`) should now be able to log in. And only have read access.
* The user `administrator` (password is also `dogood`) should be able to edit. This is because (s)he is in the ldap `desk-admin` group.


# LDAP roles without the groups

In this exercise we imported first some groups, and then assigned the role we want to the group. We could have done this directly, by mapping an LDAP group to a Keycloak Role.
To do this, remove the `group-ldap-mapper` and instead add a `role-ldap-mapper`. We'll leave the details to you, it should be trivial to map it based on the settings above. 


# Additional tips

## Running glauth on a Mac
For Intel Macs, you need the 'glauth-darwin-amd64' binary. If you try to run it from the command line, and you get a message saying: *"glauth-darwin-amd64” cannot be opened because the developer cannot be verified.*, you need to do the following to trust that binary:

* Open System Settings
* Go to Security & Privacy
* Scroll down a bit until you see the same message with an `Allow Anyway` button
* Press that button, and possibly authenticate yourself :)

## Keycloak logging
To see a bit more logging, you can enable TRACE logging for the LDAP storage provider. To do so, run keycloak with:

`bin/kc.sh start-dev --http-port 8888 --log-level=org.keycloak.storage.ldap:TRACE`


## LDAP query tools

You can download an LDAP query tool, e.g. the Mac has ldapsearch on the command line by default. Using that, you can simulate the queries Keycloak does:

E.g., Keycloak should query the users with something like this:
`ldapsearch -LLL -H ldap://localhost:3893 -D cn=serviceuser,ou=svcaccts,dc=first8,dc=com -w mysecret -x -b "ou=users,dc=first8,dc=com" "(&(objectclass=posixAccount))"`

And for the groups:
`ldapsearch -LLL -H ldap://localhost:3893 -D cn=serviceuser,ou=svcaccts,dc=first8,dc=com -w mysecret -x -b "ou=groups,dc=first8,dc=com" "(&(objectclass=groupOfUniqueNames))"`

## Accessing the keycloak database
By default, keycloak runs on a h2 database, located in `data/h2/keycloakdb.mv.db`. 
If you need to see what is in the database, use e.g. DBeaver or your IDE to connect to an embedded h2 database. A JDBC url like this `jdbc:h2:/<KEYCLOAK_LOCATION>/data/h2/keycloakdb.mv.db`  with username 'sa' and password 'password' should do the trick. Note, you probably cannot access the database while keycloak is running due to locking issues.
