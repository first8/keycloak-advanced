# Add a custom Event Listener Module

Let's extend the functionality of Keycloak. Sometimes you want an action happen when a specified action has been done. For this you can use an Event Listener. Most things that happen in Keycloak are events, for example users logging in or out, or admins making changes.

We will use the custom phonebook app to demonstrate some of the examples.

## Example event listener

We have created a custom event listener as an example that we will use. This listener simply outputs every event to stdout.

Git clone the sample event listener repo:
```
git clone https://bitbucket.org/first8/example-event-listener.git
```

Build the maven package:
```
mvn clean package
```


Check the code in `SampleEventListenerProvider.java` to get a feeling for how this is implemented. Ultimately, it is simply implementing the `EventListenerProvider` interface and then use your imagination on what you want to do with the events. In this case, we were not very creative:

```
    @Override
    public void onEvent(Event event) {
        System.out.println("Event Occurred:" + toString(event));
    }

    @Override
    public void onEvent(AdminEvent adminEvent, boolean b) {
        System.out.println("Admin Event Occurred:" + toString(adminEvent));
    }

```

## Deploy

Deploy the build artifact (target/event-listener.jar) as shown in the previous assignment.

####Alternative deployment
Alternatively you can deploy as a module by running:

```
/bin/jboss-cli.sh --command="module add --name=nl.first8.workshop.rhsso.examples.event-listener --resources=../../source/sample_event_listener/target/event-listener.jar --dependencies=org.keycloak.keycloak-core,org.keycloak.keycloak-server-spi,org.keycloak.keycloak-server-spi-private"
```
After above command you need to add configuration of this module to `standalone/configuration/standalone.xml`. In this file search for `<subsystem xmlns="urn:jboss:domain:keycloak-server:1.1">`

And add an extra provider configuration as shown below.

```
  <provider>module:nl.first8.workshop.rhsso.examples.event-listener</provider>
```

### Have a look at the code

If you have a look at the code, you can see that there are a few files required.

* the `SampleEventListenerProvider.java` is the actual listener that we implemented
* the `SampleEventListenerProviderFactory.java` is a factory for those listeners, it creates the listeners as Keycloak needs them
* the `META-INF/services/org.keycloak.events.EventListenerProviderFactory` tells Keycloak (or rather the application server) that there is a factory that it should use of the type `EventListenerProviderFactory`. The content is the actual classname of our factory, or multiple classnames if you have multiple factories
* the `META-INF/jboss-deployment-structure.xml` is a JBoss config file that list the dependencies for our listener. That is, if you want to use libraries that are not packaged with the jar you provided (e.g. dependencies marked as 'provided' in Maven).


### Configure

To configure the event listener:

* In the workshop realm: Go to `Events` under `Manage`.
* Select the Config tab
* Select the event Listener (click on the box)
  * _Note:_ Sometimes you need to refresh the page for the new event Listener to show up
* `Save`

Now you will see in the system.out the logging of events as this is the current functionality of the example event listener that you have added:
```
14:04:22,588 INFO  [stdout] (default task-3) Admin Event Occurred:operationType=UPDATE[...]
```

## Test it

You can test this further by e.g. logging in and out or simply refreshing the page.



## Add parameters
 
The example event listener can be configured to exclude certain events. Since on a busy server you may have quite a few events, it is wise to not spam your logs too much. For that, we can add parameters to our custom listener so we can configure which events to ignore. For example, to exclude REFRESH_TOKEN and CODE_TO_TOKEN events, follow these steps:

First, we need to add some code to our Factory to parse these values and pass them along to the listener:

```
public class SampleEventListenerProviderFactory ... {
    private Set<EventType> excludedEvents;
    private Set<OperationType> excludedAdminOperations;
    
    @Override
    public EventListenerProvider create(KeycloakSession keycloakSession) {
        return new SampleEventListenerProvider(excludedEvents, excludedAdminOperations);
    }

    @Override
    public void init(Config.Scope config) {
        String excludes = config.get("exclude-events");

        if (excludes != null) {

            excludedEvents = new HashSet<>();
            String[] excludedEventsArray=excludes.split(" ");
            for (String e : excludedEventsArray) {
                excludedEvents.add(EventType.valueOf(e));
            }
        }

        String excludesOperations = config.get("excludes-operations");

        if (excludesOperations != null) {
            String[] excludesOperationsArray=excludesOperations.split(" ");
            excludedAdminOperations = new HashSet<>();
            for (String e : excludesOperationsArray) {
                excludedAdminOperations.add(OperationType.valueOf(e));
            }
        }
    }
...

}

```

And, in our Listener, we need to use the newly created parameters:

```
public class SampleEventListenerProvider implements EventListenerProvider {
    private final Set<EventType> excludedEvents;
    private final Set<OperationType> excludedAdminOperations;

    public SampleEventListenerProvider(Set<EventType> excludedEvents, Set<OperationType> excludedAdminOperations) {
        this.excludedEvents = excludedEvents;
        this.excludedAdminOperations = excludedAdminOperations;
    }

    @Override
    public void onEvent(Event event) {
        if (excludedEvents != null && excludedEvents.contains(event.getType())) {
            return;
        } else {
            System.out.println("Event Occurred:" + toString(event));
        }
    }

    @Override
    public void onEvent(AdminEvent adminEvent, boolean b) {
        if (excludedAdminOperations != null && excludedAdminOperations.contains(adminEvent.getOperationType())) {
            return;
        } else {
            System.out.println("Admin Event Occurred:" + toString(adminEvent));
        }
    }
...

```

Now (re)start keycloak, but this time using the following command, which passes the events to be excluded as parameters:

* PowerShell:

```
bin/kc.bat --verbose --spi-events-listener-example-event-listener-exclude-events='REFRESH_TOKEN CODE_TO_TOKEN' start-dev
  
```

* Command Prompt:

```
bin\kc.bat --spi-events-listener-example-event-listener-exclude-events="REFRESH_TOKEN CODE_TO_TOKEN" start-dev

```


## Change the Event Listener

For the next exercise we want to be able to do some monitoring of Keycloak. For that, we need to change the event listener to count the times an event happens. This exercise is left for the reader ;)

So:

* create a static Map in the event listener that keeps track of the times a unique event happens (static, because listeners will be created and destroyed at will)
* for now, simply leave it in memory, no need to store it somewhere more permanent
* for debugging purposes, log the current count to stdout
* compile and deploy the new event listener and see if it works


