# Add a Rest End Point

As we have seen we can implement extra functionality for Keycloak by implementing
specific interfaces.

For a Rest Service we need to implement `RealmResourceProvider` and its Factory `RealmResourceProviderFactory`.

Example:

```
public class HelloEndpoint implements RealmResourceProvider {

    // The ID of the provider is also used as the name of the endpoint
    public final static String ID = "hello";

}
```

```
public class HelloEndpointFactory implements RealmResourceProviderFactory {
    @Override
    public RealmResourceProvider create(KeycloakSession session) {
        return new HelloEndpoint();
    }

    @Override
    public void init(Config.Scope config) {
    }

    @Override
    public void postInit(KeycloakSessionFactory factory) {
        // nothing to do
    }

    @Override
    public void close() {
        // nothing to close
    }

    @Override
    public String getId() {
        return HelloEndpoint.ID;
    }
}
```

Can you now implement a REST endpoint that returns Hello World?
   
* *Hint:* Skeleton project can be cloned using: `git clone https://bitbucket.org/first8/rest-hello-world.git`

* Remember that you need to add the jar to Keycloak, using one of the methods described in the previous assignments.
* Check the logging to see if your module is correctly loaded, look for:
`14:51:32,955 INFO  [org.jboss.as.server] (ServerService Thread Pool -- 31) WFLYSRV0010: Deployed "rest-hello-world.jar" (runtime-name : "rest-hello-world.jar")` or something similar
* If correctly deployed, see if you can call your API on: 
```
http://localhost:8080/auth/realms/workshop/hello
```


## Combine with Event listener

Now in a previous exercise we've created an Event Listener that tracked the times events happened. We now should be able to display those tracked events in a Rest Endpoint.

Adapt your REST endpoint provider you have just created to return the event counts.

Hint: A project that uses Prometheus to track events can be cloned using: `git clone https://bitbucket.org/first8/rest-metrics.git`. You will still need to add an endpoint.


